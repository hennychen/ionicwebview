# ionicwebview
ionic  webview In App Browser
In App Browser
Improve this doc

Launches in app Browser

Repo: https://github.com/apache/cordova-plugin-inappbrowser

Installation

Install the Cordova and Ionic Native plugins:
$ ionic cordova plugin add cordova-plugin-inappbrowser
$ npm install --save @ionic-native/in-app-browser
Add this plugin to your app's module
Supported platforms

AmazonFire OS
Android
BlackBerry 10
Browser
Firefox OS
iOS
macOS
Ubuntu
Windows
Windows Phone
Usage

import { InAppBrowser } from '@ionic-native/in-app-browser';

constructor(private iab: InAppBrowser) { }


...


const browser = this.iab.create('https://ionicframework.com/');

browser.executeScript(...);
browser.insertCSS(...);
browser.close();
Instance Members

create(url, target, options)
Opens a URL in a new InAppBrowser instance, the current browser instance, or the system browser.

Param	Type	Details
url	string	
The URL to load.
target	string	
The target in which to load the URL, an optional parameter that defaults to _self.
options	string	
Options for the InAppBrowser. Optional, defaulting to: location=yes. The options string must not contain any blank space, and each feature's name/value pairs must be separated by a comma. Feature names are case insensitive.
Returns: InAppBrowserObject

InAppBrowserObject

Instance Members

show()
Displays an InAppBrowser window that was opened hidden. Calling this has no effect if the InAppBrowser was already visible.

close()
Closes the InAppBrowser window.

hide()
Hides an InAppBrowser window that is currently shown. Calling this has no effect if the InAppBrowser was already hidden.

executeScript(script)
Injects JavaScript code into the InAppBrowser window.

Param	Type	Details
script	Object	
Details of the script to run, specifying either a file or code key.
Returns: Promise<any>

insertCSS(css)
Injects CSS into the InAppBrowser window.

Param	Type	Details
css	Object	
Details of the script to run, specifying either a file or code key.
Returns: Promise<any>

on(event)
A method that allows you to listen to events happening in the browser.

Param	Type	Details
event	string	
Name of the event
Returns: Observable<InAppBrowserEvent> Returns back an observable that will listen to the event on subscribe, and will stop listening to the event on unsubscribe.

InAppBrowserEvent

Param	Type	Details
type	string	
the eventname, either loadstart, loadstop, loaderror, or exit.
url	string	
the URL that was loaded.
code	number	
the error code, only in the case of loaderror.
message	string	
the error message, only in the case of loaderror.
InAppBrowserOptions

Param	Type	Details
location	'yes' | 'no'	
Set to yes or no to turn the InAppBrowser's location bar on or off.
(optional)
hidden	'yes' | 'no'	
Set to yes to create the browser and load the page, but not show it. The loadstop event fires when loading is complete. Omit or set to no (default) to have the browser open and load normally.
(optional)
clearcache	'yes'	
Set to yes to have the browser's cookie cache cleared before the new window is opened.
(optional)
clearsessioncache	'yes'	
Set to yes to have the session cookie cache cleared before the new window is opened.
(optional)
zoom	'yes' | 'no'	
(Android Only) set to yes to show Android browser's zoom controls, set to no to hide them. Default value is yes.
(optional)
hardwareback	'yes' | 'no'	
Set to yes to use the hardware back button to navigate backwards through the InAppBrowser's history. If there is no previous page, the InAppBrowser will close. The default value is yes, so you must set it to no if you want the back button to simply close the InAppBrowser.
(optional)
mediaPlaybackRequiresUserAction	'yes' | 'no'	
Set to yes to prevent HTML5 audio or video from autoplaying (defaults to no).
(optional)
shouldPauseOnSuspend	'yes' | 'no'	
(Android Only) Set to yes to make InAppBrowser WebView to pause/resume with the app to stop background audio (this may be required to avoid Google Play issues)
(optional)
closebuttoncaption	string	
(iOS Only) Set to a string to use as the Done button's caption. Note that you need to localize this value yourself.
(optional)
disallowoverscroll	'yes' | 'no'	
(iOS Only) Set to yes or no (default is no). Turns on/off the UIWebViewBounce property.
(optional)
toolbar	'yes' | 'no'	
(iOS Only) Set to yes or no to turn the toolbar on or off for the InAppBrowser (defaults to yes)
(optional)
enableViewportScale	'yes' | 'no'	
(iOS Only) Set to yes or no to prevent viewport scaling through a meta tag (defaults to no).
(optional)
allowInlineMediaPlayback	'yes' | 'no'	
(iOS Only) Set to yes or no to allow in-line HTML5 media playback, displaying within the browser window rather than a device-specific playback interface. The HTML's video element must also include the webkit-playsinline attribute (defaults to no)
(optional)
keyboardDisplayRequiresUserAction	'yes' | 'no'	
(iOS Only) Set to yes or no to open the keyboard when form elements receive focus via JavaScript's focus() call (defaults to yes).
(optional)
suppressesIncrementalRendering	'yes' | 'no'	
(iOS Only) Set to yes or no to wait until all new view content is received before being rendered (defaults to no).
(optional)
presentationstyle	'pagesheet' | 'formsheet' | 'fullscreen'	
(iOS Only) Set to pagesheet, formsheet or fullscreen to set the presentation style (defaults to fullscreen).
(optional)
transitionstyle	'fliphorizontal' | 'crossdissolve' | 'coververtical'	
(iOS Only) Set to fliphorizontal, crossdissolve or coververtical to set the transition style (defaults to coververtical).
(optional)
toolbarposition	'top' | 'bottom'	
(iOS Only) Set to top or bottom (default is bottom). Causes the toolbar to be at the top or bottom of the window.
(optional)
fullscreen	'yes'	
(Windows only) Set to yes to create the browser control without a border around it. Please note that if location=no is also specified, there will be no control presented to user to close IAB window.
(optional)